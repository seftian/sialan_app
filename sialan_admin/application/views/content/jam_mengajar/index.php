<?php section('css') ?>
<?php endsection('') ?>

<?php section('js') ?>
<?php endsection('') ?>

<?php section('script') ?>
<?php endsection('') ?>

<?php section('content') ?>
<div class="box box-sialan">	
	<div class="box-header with-border">
      	<h3 class="box-title"><i class="fa fa-clock-o"></i> || Jam Mengajar</h3>
    </div>
	<div class="box-body">
		<form>
			<div class="form-group col-lg-4">
				<div class="input-group input-group-sm">
					<select class="form-control" name="saya">
						<option value="#">Hari Ini</option>
						<option value="#">Senin</option>
						<option value="#">Selasa</option>
						<option value="#">Rabu</option>
						<option value="#">Kamis</option>
						<option value="#">Jum'at</option>
						<option value="#">Sabtu</option>
						<option value="#">Minggu</option>
					</select>
					<span class="input-group-btn">
						<button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Filter</button>
					</span>
				</div>
			</div>
		</form>
		<div class="col-lg-12">
			<table class="table table-condensed table-responsive table-hover" id="example1">
			<legend class="legend">Hari Ini</legend>
				<thead>
					<tr>
						<th class="text-center">Jam</th>
						<th>Mapel</th>	
						<th class="text-center">Kelas</th>
						<th class="text-center">Jurusan</th>
						<th class="text-center">Ruang</th>					
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="text-center">07.00 - 07.45</td>
						<td>Bahasa Indonesia</td>
						<td class="text-center">X</td>
						<td class="text-center">RPL</td>
						<td class="text-center"><a href="<?= base_url() ?>boking"><i class="fa fa-exclamation-circle"> belum boking</i></a></td>
					</tr>
					<tr>
						<td class="text-center">07.45 - 08.30</td>
						<td>Bahasa Inggris</td>
						<td class="text-center">XI</td>
						<td class="text-center">RPL</td>
						<td class="text-center"><b>56</b></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
<?php endsection('') ?>

<?php getview('layouts/theme') ?>