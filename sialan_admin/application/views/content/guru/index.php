<?php section('css') ?>
<link rel="stylesheet" href="<?= base_url() ?>dist/plugins/datatables/dataTables.bootstrap.css">
<?php endsection('') ?>

<?php section('js') ?>
<script src="<?= base_url() ?>dist/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>dist/plugins/datatables/dataTables.bootstrap.js"></script> 
<?php endsection('') ?>

<?php section('script') ?>
<script type="text/javascript">
	$(function() {
	    $("#example1").dataTable();
	});
</script>
<?php endsection('') ?>

<?php section('content') ?>
	<div class="alert alert-success fade in">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		Pesan Alert Sukses
	</div>
	<div class="alert alert-info">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		Pesan Alert Info
	</div>
	<div class="alert alert-warning">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		Pesan Alert Peringatan
	</div>
	<div class="alert alert-danger fade in">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		Pesan Alert Gagal
	</div>

	<div class="box box-sialan">
		<div class="box-header with-border">
          	<h3 class="box-title"><i class="fa fa-database"></i> || Guru</h3>
        </div>
		<div class="box-body">
			<form>
				<div class="col-lg-12">
					<legend>Input Guru</legend>
				</div>
				<div class="col-lg-6">
					<div class="form-group">
						<label>Nama</label>
						<input type="text" class="form-control"  placeholder="Masukkan Nama">
					</div>
					<div class="form-group">
						<label>Jenis Kelamin</label>
						<select class="form-control">
							<option value="">-</option>
							<option value="">Laki-Laki</option>
							<option value="">Perempuan</option>
						</select>
					</div>
					<div class="form-group">
						<label>Alamat</label>
						<textarea class="form-control" placeholder="Masukkan Alamat"></textarea>
					</div>
				</div>	
				<div class="col-lg-6">
					<div class="form-group">
						<label>NIK</label>
						<input type="text" class="form-control"  placeholder="Masukkan NIK">
					</div>
					<div class="form-group">
						<label>No.HP</label>
						<input type="text" class="form-control"  placeholder="Masukkan No.Hp">
					</div>
					<div class="form-group">
						<label>Password</label>
						<input type="Password" class="form-control" placeholder="Masukkan Password">
					</div>
				</div>
				<div class="col-lg-12">
					<button type="submit" class="btn btn-success"><i class="fa fa-send"></i> Kirim</button>
				</div>
			</form>
			&nbsp;
			<br>
			<hr class="hr">
			&nbsp;
			<br>
			<div class="col-lg-12">
				<legend class="legend">Tabel Guru</legend>
				<table class="table table-condensed table-responsive table-hover" id="example1">
					<thead>
						<tr>
							<th class="text-center">NIK</th>
							<th class="text-center">NAMA</th>
							<th class="text-center">ALAMAT</th>
							<th class="text-center">NO.HP</th>
							<th class="text-center" width="140">OPSI</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>CONTOH</td>
							<td>CONTOH</td>
							<td>CONTOH</td>
							<td>CONTOH</td>
							<td class="text-center">
								<a href="#" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="bottom" title="Edit Data"><i class="fa fa-edit"></i></a>
								<a href="#" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="bottom" title="Hapus Data"><i class="fa fa-trash"></i></a>
							</td>
						</tr>
						<tr>
							<td>CONTOH</td>
							<td>CONTOH</td>
							<td>CONTOH</td>
							<td>CONTOH</td>
							<td class="text-center">
								<a href="#" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="bottom" title="Edit Data"><i class="fa fa-edit"></i></a>
								<a href="#" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="bottom" title="Hapus Data"><i class="fa fa-trash"></i></a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
<?php endsection('') ?>

<?php getview('layouts/theme') ?>