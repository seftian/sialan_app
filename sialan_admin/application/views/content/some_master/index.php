<?php section('css') ?>
<link rel="stylesheet" href="<?= base_url() ?>dist/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<?php endsection('') ?>

<?php section('js') ?>
<!-- InputMask -->
<script src="<?= base_url() ?>dist/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?= base_url() ?>dist/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script src="<?= base_url() ?>dist/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<?php endsection('') ?>

<?php section('script') ?>
<script type="text/javascript">
	$(function() {
	    $("[data-mask]").inputmask();
	});
</script>
<?php endsection('') ?>

<?php section('content') ?>
	<div class="alert alert-success fade in">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		Pesan Alert Sukses
	</div>
	<div class="alert alert-info">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		Pesan Alert Info
	</div>
	<div class="alert alert-warning">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		Pesan Alert Peringatan
	</div>
	<div class="alert alert-danger fade in">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		Pesan Alert Gagal
	</div>

	<form>
		<div class="box box-sialan">
			<div class="box-header with-border">
              	<h3 class="box-title"><i class="fa fa-database"></i> || Some Master In One</h3>
            </div>
			<div class="box-body">
				<div class="col-lg-12">
					<div class="row">
						<div class="col-lg-6">
							<legend>Input Kelas</legend>
							<form>
								<input type="text" name="" value="" data-role="tagsinput" placeholder="Masukkan Kelas"><br>
								<code>*Silahkan klik tombol TAB ( <i class="fa fa-exchange"></i> ) untuk menambah inputan data atau silahkan klik area di luar form</code><br><br>	
								<button type="submit" class="btn btn-sm btn-success"><i class="fa fa-send"></i> Kirim</button>
							</form>
						</div>

						<div class="col-lg-6">
							<table class="table table-responsive table-condensed table-hover data">
							<legend class="legend">Tabel Kelas</legend>
								<thead>
									<tr>
										<th>KELAS</th>						
										<th  class="text-center" width="140">OPSI</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>X</td>
										<td class="text-center">
											<a href="#" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="bottom" title="Edit Data"><i class="fa fa-edit"></i></a>
											<a href="#" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="bottom" title="Hapus Data"><i class="fa fa-trash"></i></a>
										</td>
									</tr>
									<tr>
										<td>XI</td>
										<td class="text-center">
											<a href="#" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="bottom" title="Edit Data"><i class="fa fa-edit"></i></a>
											<a href="#" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="bottom" title="Hapus Data"><i class="fa fa-trash"></i></a>
										</td>
									</tr>
									<tr>
										<td>XII</td>
										<td class="text-center">
											<a href="#" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="bottom" title="Edit Data"><i class="fa fa-edit"></i></a>
											<a href="#" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="bottom" title="Hapus Data"><i class="fa fa-trash"></i></a>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
					
				<hr class="hr">

				<div class="col-lg-12">
					<div class="row">
						<div class="col-lg-6">
							<legend>Input Jurusan</legend>
							<form>
								<input name="" value="" data-role="tagsinput" placeholder="Masukkan Jurusan"><br>
								<code>*Silahkan klik tombol TAB ( <i class="fa fa-exchange"></i> ) untuk menambah inputan data atau silahkan klik area di luar form</code><br><br>		
								<button type="submit" class="btn btn-sm btn-success"><i class="fa fa-send"></i> Kirim</button>
							</form>
						</div>

						<div class="col-lg-6">
							<table class="table table-responsive table-condensed table-hover">
							<legend class="legend">Tabel Jurusan</legend>
								<thead>
									<tr>
										<th>JURUSAN</th>						
										<th  class="text-center" width="140">OPSI</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>IPA</td>
										<td class="text-center">
											<a href="#" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="bottom" title="Edit Data"><i class="fa fa-edit"></i></a>
											<a href="#" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="bottom" title="Hapus Data"><i class="fa fa-trash"></i></a>
										</td>
									</tr>
									<tr>
										<td>IPS</td>
										<td class="text-center">
											<a href="#" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="bottom" title="Edit Data"><i class="fa fa-edit"></i></a>
											<a href="#" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="bottom" title="Hapus Data"><i class="fa fa-trash"></i></a>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				
				<hr class="hr">

				<div class="col-lg-12" style="margin-top: 30px;">
					<div class="row">
						<div class="col-lg-6">
							<legend>Input Jam</legend>
							<form>
								<input type="text" class="form-control" data-inputmask='"mask": "99:99:99"' data-mask><br>				
								<button type="submit" class="btn btn-sm btn-success"><i class="fa fa-send"></i> Kirim</button>
							</form>
						</div>

						<div class="col-lg-6">
							<table class="table table-responsive table-condensed table-hover">
							<legend class="legend">Tabel Jam</legend>
								<thead>
									<tr>
										<th>JAM</th>						
										<th  class="text-center" width="140">OPSI</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>07:00:00</td>
										<td class="text-center">
											<a href="#" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="bottom" title="Edit Data"><i class="fa fa-edit"></i></a>
											<a href="#" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="bottom" title="Hapus Data"><i class="fa fa-trash"></i></a>
										</td>
									</tr>
									<tr>
										<td>07:45:00</td>
										<td class="text-center">
											<a href="#" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="bottom" title="Edit Data"><i class="fa fa-edit"></i></a>
											<a href="#" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="bottom" title="Hapus Data"><i class="fa fa-trash"></i></a>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<div class="col-lg-12">
					<button type="submit" class="btn btn-success"><i class="fa fa-send"></i> Kirim Semua Data</button>
					<a href="#" class="btn btn-danger"><i class="fa fa-trash"></i> Hapus Semua Data</a>
				</div>
			</div>	
		</div>
	</form>
<?php endsection('') ?>

<?php getview('layouts/theme') ?>