<?php section('css') ?>
<link rel="stylesheet" href="<?= base_url() ?>dist/plugins/datatables/dataTables.bootstrap.css">
<?php endsection('') ?>

<?php section('js') ?>
<script src="<?= base_url() ?>dist/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>dist/plugins/datatables/dataTables.bootstrap.js"></script> 
<?php endsection('') ?>

<?php section('script') ?>
<script type="text/javascript">
	$(function() {
	    $("#example1").dataTable();
	});
</script>
<?php endsection('') ?>

<?php section('content') ?>
<div class="box box-sialan">
	<div class="box-header with-border">
      	<h3 class="box-title"><i class="fa fa-bookmark-o"></i> || Boking</h3>
    </div>
	<div class="box-body">
		<form>
			<div class="form-group col-lg-4">
				<div class="input-group input-group-sm">
					<select class="form-control" name="saya">
						<option>-- Jam Mulai --</option>
						<option value="#">07.00</option>
						<option value="#">07.45</option>
						<option value="#">08.30</option>
						<option value="#">09.15</option>
					</select>
					<div class="input-group-addon">
						s/d
					</div>
					<select class="form-control" name="saya">
						<option>-- Jam Selesai --</option>
						<option value="#">07.00</option>
						<option value="#">07.45</option>
						<option value="#">08.30</option>
						<option value="#">09.15</option>
					</select>
				</div>
			</div>
			<div class="form-group form-group-sm col-lg-3">
				<select class="form-control" name="saya">
					<option>-- Status --</option>
					<option value="#">Tampilkan Semua</option>
					<option value="#">S. Boking</option>
					<option value="#">B. Boking</option>
				</select>
			</div>
			<div class="form-group form-group-sm col-lg-5">
				<button type="submit" class="btn btn-sm btn-info"><i class="fa fa-search"></i> Filter</button>
			</div>
			<div class="form-group form-group-sm col-lg-12">
				<code>* S.= Sudah / B.= Belum</code>
			</div>
		</form>
		<div class="col-lg-12">
			<table class="table table-condensed table-responsive table-hover" id="example1">
			<legend class="legend">Tabel Boking</legend>
				<thead>
					<tr>
						<th class="text-center">Ruang</th>
						<th class="text-center">Jam Mengajar</th>	
						<th class="text-center">Pengajar</th>
						<th class="text-center">Mapel</th>
						<th class="text-center">Kelas</th>
						<th class="text-center">Status</th>					
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="text-center">1</td>
						<td class="text-center">07.00 - 07.45</td>
						<td class="text-center">Sugiarto .ST</td>
						<td class="text-center">Teknik Komputer</td>
						<td class="text-center">X</td>
						<td class="text-center">
							<span class="btn btn-sm btn-default">S. Boking</span>
							<button type="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#modalUnboking">Unboking</button>
						</td>
					</tr>
					<tr>
						<td class="text-center">1</td>
						<td class="text-center">07.45 - 08.30</td>
						<td class="text-center">Sugiarto .ST</td>
						<td class="text-center">Teknik Komputer</td>
						<td class="text-center">X</td>
						<td class="text-center">
							<span class="btn btn-sm btn-default">S. Boking</span>
							<button type="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#modalUnboking">Unboking</button>
						</td>
					</tr>
					<tr>
						<td class="text-center">1</td>
						<td class="text-center">08.30 - 09.15</td>
						<td class="text-center">-</td>
						<td class="text-center">-</td>
						<td class="text-center">-</td>
						<td class="text-center">
							<button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#modalBelumBoking">B. Boking</button>
						</td>
					</tr>
					<tr>
						<td class="text-center">1</td>
						<td class="text-center">09.15 - 10.00</td>
						<td class="text-center">-</td>
						<td class="text-center">-</td>
						<td class="text-center">-</td>
						<td class="text-center">
							<button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#modalBelumBoking">B. Boking</button>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="modal fade" id="modalBelumBoking" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Form Boking</h4>
			</div>
			<form>
				<div class="modal-body">
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group form-group-sm">
								<label>NIK</label>
								<input type="text" name="" value="" class="form-control" disabled>
							</div>
							<div class="form-group form-group-sm">
								<label>Nama</label>
								<input type="text" name="" value="" class="form-control" disabled>
							</div>
							<div class="form-group form-group-sm">
								<label>Mata Pelajaran</label>
								<select class="form-control">
									<option>-- Silahkan Pilih Mapel --</option>
									<option value="">Bhs. Indonesia</option>
									<option value="">Bhs. Inggris</option>
								</select>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group form-group-sm">
								<label>Ruang</label>
								<input type="text" name="" value="" class="form-control" disabled>
							</div>
							<div class="form-group form-group-sm">
								<label>Untuk Kelas</label>
								<select class="form-control">
									<option>-- Silahkan Pilih Kelas --</option>
									<option value="">X</option>
									<option value="">XI</option>
									<option value="">XII</option>
								</select>
							</div>
							<div class="form-group form-group-sm">
								<label>Untuk Jurusan</label>
								<select class="form-control">
									<option>-- Silahkan Pilih Jurusan --</option>
									<option value="">RPL</option>
									<option value="">TKJ</option>
									<option value="">MM</option>
								</select>
							</div>
							<div class="form-group form-group-sm">
								<label>Jam Mulai</label>
								<input type="text" name="" value="" class="form-control" disabled>
							</div>
							<div class="form-group form-group-sm">
								<label>Jam Selesai</label>
								<select class="form-control">
									<option>-- Silahkan Pilih End Jam --</option>
									<option value="">07.45</option>
									<option value="">08.30</option>
									<option value="">09.15</option>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
					<button type="button" class="btn btn-success"><i class="fa fa-bookmark-o"></i> Boking</button>
				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modalUnboking" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Form Boking</h4>
			</div>
			<form>
				<div class="modal-body">
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group form-group-sm">
								<label>NIK</label>
								<input type="text" name="" value="" class="form-control" disabled>
							</div>
							<div class="form-group form-group-sm">
								<label>Nama</label>
								<input type="text" name="" value="" class="form-control" disabled>
							</div>
							<div class="form-group form-group-sm">
								<label>Mata Pelajaran</label>
								<select class="form-control">
									<option>-- Silahkan Pilih Mapel --</option>
									<option value="">Bhs. Indonesia</option>
									<option value="">Bhs. Inggris</option>
								</select>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group form-group-sm">
								<label>Ruang</label>
								<input type="text" name="" value="" class="form-control" disabled>
							</div>
							<div class="form-group form-group-sm">
								<label>Untuk Kelas</label>
								<select class="form-control">
									<option>-- Silahkan Pilih Kelas --</option>
									<option value="">X</option>
									<option value="">XI</option>
									<option value="">XII</option>
								</select>
							</div>
							<div class="form-group form-group-sm">
								<label>Untuk Jurusan</label>
								<select class="form-control">
									<option>-- Silahkan Pilih Jurusan --</option>
									<option value="">RPL</option>
									<option value="">TKJ</option>
									<option value="">MM</option>
								</select>
							</div>
							<div class="form-group form-group-sm">
								<label>Jam Mulai</label>
								<input type="text" name="" value="" class="form-control" disabled>
							</div>
							<div class="form-group form-group-sm">
								<label>Jam Selesai</label>
								<select class="form-control">
									<option>-- Silahkan Pilih End Jam --</option>
									<option value="">07.45</option>
									<option value="">08.30</option>
									<option value="">09.15</option>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
					<button type="button" class="btn btn-warning"><i class="fa fa-bookmark-o"></i> Unboking</button>
				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<?php endsection('') ?>

<?php getview('layouts/theme') ?>