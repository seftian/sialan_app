<?php section('css') ?>
<link rel="stylesheet" href="<?= base_url() ?>dist/plugins/datatables/dataTables.bootstrap.css">
<link rel="stylesheet" href="<?= base_url() ?>dist/plugins/select2/select2.min.css">
<?php endsection('') ?>

<?php section('js') ?>
<script src="<?= base_url() ?>dist/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>dist/plugins/datatables/dataTables.bootstrap.js"></script> 
<script src="<?= base_url() ?>dist/plugins/select2/select2.full.min.js"></script>
<?php endsection('') ?>

<?php section('script') ?>
<script type="text/javascript">
	$(function() {
	    $(".select2").select2();
	    $("#example1").dataTable();
	});
</script>
<?php endsection('') ?>

<?php section('content') ?>
<div class="alert alert-success fade in">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	Pesan Alert Sukses
</div>
<div class="alert alert-info">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	Pesan Alert Info
</div>
<div class="alert alert-warning">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	Pesan Alert Peringatan
</div>
<div class="alert alert-danger fade in">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	Pesan Alert Gagal
</div>


<div class="box box-sialan">
	<div class="box-header with-border">
      	<h3 class="box-title"><i class="fa fa-database"></i> || Mata Pelajaran</h3>
    </div>	
	<div class="box-body">
		<div class="col-lg-5">
			<legend>Input Mapel</legend>
			<form>
				<div class="form-group">
					<label>Mapel</label>
					<input type="text" class="form-control" placeholder="Masukkan Mapel">
				</div>
				<div class="form-group">
					<label>Guru</label>
					<select class="form-control select2" multiple="multiple" data-placeholder="Masukkan Guru" style="width: 100%;">
		                <option>M Alfandi .Spd</option>
		                <option>Ria Sinia .Sag</option>
	                </select>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-success"><i class="fa fa-send"></i> Kirim</button>
				</div>
			</form>
		</div>
		<div class="col-lg-7">
			<table class="table table-condensed table-responsive table-hover" id="example1">
			<legend class="legend">Tabel Mapel</legend>
				<thead>
					<tr>
						<th>Mapel</th>
						<th>Guru</th>						
						<th width="140" class="text-center">OPSI</th>						
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Bahasa Indonesia</td>
						<td>M Alfandi .Spd</td>
						<td class="text-center">
							<a href="#" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="bottom" title="Edit Data"><i class="fa fa-edit"></i></a>
							<a href="#" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="bottom" title="Hapus Data"><i class="fa fa-trash"></i></a>
						</td>
					</tr>
					<tr>
						<td>Agama Islam</td>
						<td>Ria Sinia .Sag</td>
						<td class="text-center">
							<a href="#" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="bottom" title="Edit Data"><i class="fa fa-edit"></i></a>
							<a href="#" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="bottom" title="Hapus Data"><i class="fa fa-trash"></i></a>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
<?php endsection('') ?>

<?php getview('layouts/theme') ?>