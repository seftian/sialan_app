<?php section('css') ?>
<?php endsection('') ?>

<?php section('js') ?>
<?php endsection('') ?>

<?php section('script') ?>
<?php endsection('') ?>

<?php section('content') ?>
<div class="box box-sialan">
	<div class="box-header with-border">
      	<h3 class="box-title"><i class="fa fa-calendar"></i> || Jadwal</h3>
    </div>	
	<div class="box-body">
		<div class="col-lg-4">
			<div class="form-group">
				<label>Kelas</label>
				<select class="form-control">
					<option>X</option>
		            <option>XI</option>
		            <option>XII</option>
				</select>
			</div>
			<div class="form-group">
				<label>Jurusan</label>
				<select class="form-control">
					<option>RPL</option>
		            <option>TKJ</option>
		            <option>MM</option>
				</select>
			</div>
			<div class="form-group">
				<button class="btn btn-info"><i class="fa fa-search"></i> Tampilkan</button>
			</div>
		</div>
		<div class="col-lg-12">
			<div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#senin" data-toggle="tab">Senin</a></li>
					<li><a href="#selasa" data-toggle="tab">Selasa</a></li>
					<li><a href="#rabu" data-toggle="tab">Rabu</a></li>
					<li><a href="#kamis" data-toggle="tab">Kamis</a></li>
					<li><a href="#jumat" data-toggle="tab">Jum'at</a></li>
					<li><a href="#sabtu" data-toggle="tab">Sabtu</a></li>
					<li><a href="#minggu" data-toggle="tab">Minggu</a></li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="senin">
						<form>
							<table class="table table-condensed table-responsive table-hover">
								<thead>
									<tr>
										<th class="text-center">Jam Mapel</th>
										<th class="text-center">Mapel</th>
										<th class="text-center">Guru</th>
										<th class="text-center" width="140">OPSI</th>
									</tr>
									<tr>
										<td class="text-center">
											<select class="form-control input-sm">
												<option>07.00 - 07.45</option>
											</select>
										</td>
										<td class="text-center">
											<select class="form-control input-sm">
												<option>Bahasa Indonesia</option>
											</select>
										</td>
										<td class="text-center">
											<select class="form-control input-sm">
												<option>M Alfandi .Spd</option>
											</select>
										</td>
										<td class="text-center" width="140">
											<button type="submit" class="btn btn-sm btn-success"><i class="fa fa-send"></i> Kirim</button>
										</td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="text-center">07.00 - 07.45</td>
										<td>Bahasa Indonesia</td>
										<td>M Alfandi .Spd</td>
										<td class="text-center">
											<a href="#" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="bottom" title="Edit Data"><i class="fa fa-edit"></i></a>
											<a href="#" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="bottom" title="Hapus Data"><i class="fa fa-trash"></i></a>
										</td>
									</tr>
								</tbody>
							</table>
						</form>
					</div>
					<div class="tab-pane" id="selasa">
						<form>
							<table class="table table-condensed table-responsive table-hover">
								<thead>
									<tr>
										<th class="text-center">Jam Mapel</th>
										<th class="text-center">Mapel</th>
										<th class="text-center">Guru</th>
										<th class="text-center" width="140">OPSI</th>
									</tr>
									<tr>
										<td class="text-center">
											<select class="form-control input-sm">
												<option>07.00 - 07.45</option>
											</select>
										</td>
										<td class="text-center">
											<select class="form-control input-sm">
												<option>Matematika</option>
											</select>
										</td>
										<td class="text-center">
											<select class="form-control input-sm">
												<option>Sunaryo .Spd</option>
											</select>
										</td>
										<td class="text-center" width="140">
											<button type="submit" class="btn btn-sm btn-success"><i class="fa fa-send"></i> Kirim</button>
										</td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="text-center">07.00 - 07.45</td>
										<td>Matematika</td>
										<td>Sunaryo .Spd</td>
										<td class="text-center">
											<a href="#" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="bottom" title="Edit Data"><i class="fa fa-edit"></i></a>
											<a href="#" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="bottom" title="Hapus Data"><i class="fa fa-trash"></i></a>
										</td>
									</tr>
								</tbody>
							</table>
						</form>
					</div>
					<div class="tab-pane" id="rabu">
						<form>
							<table class="table table-condensed table-responsive table-hover">
								<thead>
									<tr>
										<th class="text-center">Jam Mapel</th>
										<th class="text-center">Mapel</th>
										<th class="text-center">Guru</th>
										<th class="text-center" width="140">OPSI</th>
									</tr>
									<tr>
										<td class="text-center">
											<select class="form-control input-sm">
												<option>07.00 - 07.45</option>
											</select>
										</td>
										<td class="text-center">
											<select class="form-control input-sm">
												<option>Bahasa Inggris</option>
											</select>
										</td>
										<td class="text-center">
											<select class="form-control input-sm">
												<option>M Alfandi .Spd</option>
											</select>
										</td>
										<td class="text-center" width="140">
											<button type="submit" class="btn btn-sm btn-success"><i class="fa fa-send"></i> Kirim</button>
										</td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="text-center">07.00 - 07.45</td>
										<td>Bahasa Inggris</td>
										<td>M Alfandi .Spd</td>
										<td class="text-center">
											<a href="#" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="bottom" title="Edit Data"><i class="fa fa-edit"></i></a>
											<a href="#" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="bottom" title="Hapus Data"><i class="fa fa-trash"></i></a>
										</td>
									</tr>
								</tbody>
							</table>
						</form>
					</div>
					<div class="tab-pane" id="kamis">
						<form>
							<table class="table table-condensed table-responsive table-hover">
								<thead>
									<tr>
										<th class="text-center">Jam Mapel</th>
										<th class="text-center">Mapel</th>
										<th class="text-center">Guru</th>
										<th class="text-center" width="140">OPSI</th>
									</tr>
									<tr>
										<td class="text-center">
											<select class="form-control input-sm">
												<option>07.00 - 07.45</option>
											</select>
										</td>
										<td class="text-center">
											<select class="form-control input-sm">
												<option>Bahasa Jawa</option>
											</select>
										</td>
										<td class="text-center">
											<select class="form-control input-sm">
												<option>Siti Badriah .Spd</option>
											</select>
										</td>
										<td class="text-center" width="140">
											<button type="submit" class="btn btn-sm btn-success"><i class="fa fa-send"></i> Kirim</button>
										</td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="text-center">07.00 - 07.45</td>
										<td>Bahasa Jawa</td>
										<td>Siti Badriah .Spd</td>
										<td class="text-center">
											<a href="#" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="bottom" title="Edit Data"><i class="fa fa-edit"></i></a>
											<a href="#" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="bottom" title="Hapus Data"><i class="fa fa-trash"></i></a>
										</td>
									</tr>
								</tbody>
							</table>
						</form>
					</div>
					<div class="tab-pane" id="jumat">
						<form>
							<table class="table table-condensed table-responsive table-hover">
								<thead>
									<tr>
										<th class="text-center">Jam Mapel</th>
										<th class="text-center">Mapel</th>
										<th class="text-center">Guru</th>
										<th class="text-center" width="140">OPSI</th>
									</tr>
									<tr>
										<td class="text-center">
											<select class="form-control input-sm">
												<option>07.00 - 07.45</option>
											</select>
										</td>
										<td class="text-center">
											<select class="form-control input-sm">
												<option>Olah Raga</option>
											</select>
										</td>
										<td class="text-center">
											<select class="form-control input-sm">
												<option>Saiful Jamal .Spd</option>
											</select>
										</td>
										<td class="text-center" width="140">
											<button type="submit" class="btn btn-sm btn-success"><i class="fa fa-send"></i> Kirim</button>
										</td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="text-center">07.00 - 07.45</td>
										<td>Olah Raga</td>
										<td>Saiful Jamal .Spd</td>
										<td class="text-center">
											<a href="#" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="bottom" title="Edit Data"><i class="fa fa-edit"></i></a>
											<a href="#" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="bottom" title="Hapus Data"><i class="fa fa-trash"></i></a>
										</td>
									</tr>
								</tbody>
							</table>
						</form>
					</div>
					<div class="tab-pane" id="sabtu">
						<form>
							<table class="table table-condensed table-responsive table-hover">
								<thead>
									<tr>
										<th class="text-center">Jam Mapel</th>
										<th class="text-center">Mapel</th>
										<th class="text-center">Guru</th>
										<th class="text-center" width="140">OPSI</th>
									</tr>
									<tr>
										<td class="text-center">
											<select class="form-control input-sm">
												<option>07.00 - 07.45</option>
											</select>
										</td>
										<td class="text-center">
											<select class="form-control input-sm">
												<option>Kesenian</option>
											</select>
										</td>
										<td class="text-center">
											<select class="form-control input-sm">
												<option>Ki Subroto Diningrat</option>
											</select>
										</td>
										<td class="text-center" width="140">
											<button type="submit" class="btn btn-sm btn-success"><i class="fa fa-send"></i> Kirim</button>
										</td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="text-center">07.00 - 07.45</td>
										<td>Kesenian</td>
										<td>Ki Subroto Diningrat</td>
										<td class="text-center">
											<a href="#" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="bottom" title="Edit Data"><i class="fa fa-edit"></i></a>
											<a href="#" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="bottom" title="Hapus Data"><i class="fa fa-trash"></i></a>
										</td>
									</tr>
								</tbody>
							</table>
						</form>
					</div>
					<div class="tab-pane" id="minggu">
						<form>
							<table class="table table-condensed table-responsive table-hover">
								<thead>
									<tr>
										<th class="text-center">Jam Mapel</th>
										<th class="text-center">Mapel</th>
										<th class="text-center">Guru</th>
										<th class="text-center" width="140">OPSI</th>
									</tr>
									<tr>
										<td class="text-center">
											<select class="form-control input-sm">
												<option>07.00 - 07.45</option>
											</select>
										</td>
										<td class="text-center">
											<select class="form-control input-sm">
												<option>Bahasa Indonesia</option>
											</select>
										</td>
										<td class="text-center">
											<select class="form-control input-sm">
												<option>M Alfandi .Spd</option>
											</select>
										</td>
										<td class="text-center" width="140">
											<button type="submit" class="btn btn-sm btn-success"><i class="fa fa-send"></i> Kirim</button>
										</td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="text-center">-</td>
										<td>-</td>
										<td>-</td>
										<td class="text-center">
											<a href="#" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="bottom" title="Edit Data"><i class="fa fa-edit"></i></a>
											<a href="#" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="bottom" title="Hapus Data"><i class="fa fa-trash"></i></a>
										</td>
									</tr>
								</tbody>
							</table>
						</form>
					</div>
					<!-- /.tab-pane -->
				</div>
				<!-- /.tab-content -->
			</div>
			<!-- nav-tabs-custom -->
		</div>
	</div>
</div>
<?php endsection('') ?>

<?php getview('layouts/theme') ?>