<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Starter</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?= base_url() ?>dist/plugins/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url() ?>dist/plugins/font-awesome/css/font-awesome.min.css">
  <!-- render css -->
  <?php render('css') ?>
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url() ?>dist/css/AdminLTE.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect.
  -->
  <link rel="stylesheet" href="<?= base_url() ?>dist/css/skins/_all-skins.css">


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <link rel="stylesheet" href="<?= base_url() ?>dist/css/style.css">
  
</head>

<body class="hold-transition skin-red sidebar-mini">
<div class="wrapper">
  <!-- Main Header -->
  <header class="main-header">
    <!-- Logo -->
    <a href="<?= base_url() ?>dashboard" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><i class="fa fa-strikethrough" style="color: #D73925;"></i><i class="fa fa-font"></i></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b style="color: #D73925;">Sialan</b>APP</span>
    </a>
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">     
          <!-- User Account Menu -->
          <li class="user user-menu">
            <!-- Menu Toggle Button -->
            <a href="javascript:;">
              <!-- The user image in the navbar-->
              <img src="<?= base_url() ?>dist/img/khafit.png" class="user-image" alt="User Image">
              <!-- hidden-xs hides the username on small devices so only the image appears. -->
              <span class="hidden-xs">Khafit Badruz Zaman</span>
            </a>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="Login"><i class="glyphicon glyphicon-log-in"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?= base_url() ?>dist/img/khafit.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Khafit Badruz Zaman</p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- Sidebar Menu -->
      <ul class="sidebar-menu">
        <li class="header">MENU</li>
        <!-- Optionally, you can add icons to the links -->        
        <li><a href="<?= base_url() ?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-database"></i> <span>Master</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= base_url() ?>some_master"><i class="fa fa-circle-o"></i> Some master in one</a></li>
            <li><a href="<?= base_url() ?>guru"><i class="fa fa-circle-o"></i> Guru</a></li>
            <li><a href="<?= base_url() ?>mapel"><i class="fa fa-circle-o"></i> Mata Pelajaran</a></li>
          </ul>
        </li>
        <li><a href="<?= base_url() ?>jadwal"><i class="fa fa-calendar"></i> <span>Jadwal</span></a></li>
        <li><a href="<?= base_url() ?>jam_mengajar"><i class="fa fa-clock-o"></i> <span>Jam Mengajar</span></a></li>
        <li><a href="<?= base_url() ?>boking"><i class="fa fa-bookmark-o"></i> <span>Boking</span></a></li>
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">

      <?php render('content') ?>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Version 1.2
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; Sialan App 2017 .</strong> All rights reserved.
  </footer>

</div>
<!-- ./wrapper -->
<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.2.3 -->
<script src="<?= base_url() ?>dist/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?= base_url() ?>dist/plugins/bootstrap/js/bootstrap.min.js"></script>
<?php render('js') ?>
<!-- AdminLTE App -->
<script src="<?= base_url() ?>dist/js/app.min.js"></script>
<script src="<?= base_url() ?>dist/js/custom.js"></script>
<?php render('script') ?>
</body>
</html>
