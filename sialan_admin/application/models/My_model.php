<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_model extends CI_Model {
	public function GetClass(){
		$data = $this->db->get('tb_kelas');
		return $data;
	}
	public function GetClass1($where = ''){
		$data = $this->db
			->select('*')
			->from('tb_kelas')
			->where('id_kelas',$where)
			->get();
		return $data;
	}
	public function add_classes($table,$data){
		$input = $this->db->insert($table,$data);
		return $input;
	}
	public function delete($table,$where){
		$del = $this->db->delete($table,$where);
		return $del;
	}
	public function Updatedata($table,$data,$where){
		$res = $this->db->update($table,$data,$where);
		return $res;
	}
	public function get_jurusan($id = ''){
		$data = $this->db->get('tb_jurusan');
		return $data;
	}
	public function get_jam($id = ''){
		$data = $this->db->get('tb_jam');
		return $data;
	}
	public function Getjurusan1($id = ''){
		$data = $this->db
			->select('*')
			->from('tb_jurusan')
			->where('id_jurusan',$id)
			->get();
		return $data;
	}
	public function get_jam1($where = ''){
		$data = $this->db
			->select('*')
			->from('tb_jam')
			->where('id_jam',$where)
			->get();
		return $data;
	}
	public function get_guru($id = ''){
		$data = $this->db->get('tb_guru');
		return $data;
	}
	public function get_guru1($where = ''){
		$data = $this->db
			->select('*')
			->from('tb_guru')
			->where('nik',$where)
			->get();
		return $data;
	}
	public function get_mapel($id = ''){
		$data = $this->db->get('tb_mapel');
		return $data;
	}
	public function get_mapel1($where = ''){
		$data = $this->db
			->select('*')
			->from('tb_mapel')
			->where('id_mapel',$where)
			->get();
		return $data;
	}
}